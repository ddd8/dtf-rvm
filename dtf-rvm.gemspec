# -*- encoding: utf-8 -*-
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'dtf-rvm/version'

Gem::Specification.new do |gem|
  gem.name          = "dtf-rvm"
  gem.version       = Dtf::Rvm::VERSION
  gem.authors       = ["David Deryl Downey"]
  gem.email         = ["me@daviddwdowney.com"]
  gem.description   = %q{DTF gem integrating RVM into DTF}
  gem.summary       = %q{DTF-RVM is a DTF sub-gem integrating RVM into the DTF platform}
  gem.homepage      = "https://dtf-gems.daviddwdowney.com/dtf-rvm"

  gem.files         = `git ls-files`.split($/)
  gem.executables   = gem.files.grep(%r{^bin/}).map{ |f| File.basename(f) }
  gem.test_files    = gem.files.grep(%r{^(test|spec|features)/})
  gem.require_paths = ["lib"]
  gem.required_ruby_version = '>= 1.9.2'

  gem.add_dependency("dtf")
  gem.add_dependency("rvm")
  gem.add_dependency("rake")
  gem.add_dependency "sqlite3"
  gem.add_dependency "json"
  gem.add_dependency "json_pure"

  gem.add_development_dependency "rspec"
  gem.add_development_dependency "fabrication"
  gem.add_development_dependency "yard"      # For generating documentation
  gem.add_development_dependency "redcarpet" # For generating YARD docs
end
