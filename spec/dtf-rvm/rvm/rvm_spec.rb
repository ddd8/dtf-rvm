require File.expand_path(File.dirname(__FILE__) + '/../../spec_helper')

describe Dtf::Rvm do

  context "Module" do

    it "should be a Module" do
      Dtf::Rvm.class.should eq(Module)
    end

  end

  context "Dependencies" do

    it "should depend on RVM gem" do
      Gem.loaded_specs['rvm'].should_not be_nil
    end

  end
end
